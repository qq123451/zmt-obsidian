---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
名词后缀，构成抽象名词，表示行为、性质、状态、情况

### 语素来源


### 同源单词
- health  n. 健康；卫生；保健；兴旺  (heal v. 康复  + [[-th]]  -> n. 健康)
- truth n. 真理（tru=true） (true adj. 真实的，正确的 + [[-th]]  ->  [[-e]] 省略 , n. 真理  )
- warmth n. 温暖，热情
- coolth n. 凉爽，凉 
- growth n. 成长，发育 
- stealth n. 秘密行动，秘密
- strength n. 力量（streng=strong） 
- breadth n. 力度（bread=broad） 
- length n. 长度（leng=long）  
- width n. 宽度（wid=wide） 
- depth n. 深度（dep=deep） 

[注]加在基数词之后，表示“第...”、兼表“...分之一”
- fourth 第四，四分之一
- fifth 第五，五分之一
- sixth 第六，六分之一
- seventh 第七，七分之一
