### 语素含义
形容词后缀
1) 加在名词之后，表示“有...的”、“如...的”、“...的”
2) 加在动词之后，表示“已...的”、“被...的”、“...了的”

### 语素来源


### 同源单词
1) 
	- coloured 有色的  
	- booted 穿靴的  
	- moneyed 有钱的  
	- dark-haired 黑发的  
	- gifted 有天才的  
	- kinde-hearted 好心的  
	- talented 有才能的  
	- conditioned 有条件的  
	- haried 有毛发的  
	- privileged 有特权的  
	- winged 有翅的  
	- skilled 熟练的  
	- beared 有胡须的  
	- balconied 有阳台的 
	- aged 年老的，...岁的  
	- horned 有角的
1) 
	- failed 已失败的 
	- fixed 被固定的  
	- liberated 解放了的  
	- determined 已决定了的  
	- retired 已退休的  
	- extended 扩展了的  
	- condensed 缩短了的 
	- confirmed 被证实了的 
	- educated 受过教育的  
	- returned 已归来的  
	- married 已婚的  
	- condemned 定了罪的  
	- restreicted 受限制的 
	- considered 考虑过的  
	- closed 关闭了的 
	- oiled 上了油的 
	- finished 完成了的  
	- wounded 受了伤的
