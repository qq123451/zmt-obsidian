---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
再,重复;回,向后;相反,反对;离开
1) 表示“向后，相反，不”
2) 表示“一再，重新”

### 语素来源


### 同源单词
1) 
	- reflect 回想;反射(re+flect弯曲→反弯曲→反射)  
	- retreat 后退,撤退(re+treat拉→拉回来→撤退)  
	- retract 缩回;收回(re+tract拉→拉回,缩回)  
	- resist 反抗,抵抗(re+sist站→反着站→反抗)  
	- reverse 反转的,颠倒的(re+verse转→反转的) 
	- revolt 反叛(re+volt转→反过转→反叛)  
	- resent 忿恨,不满(re+sent感觉→反感→不满)  
	- relinquish 不再采取行动,放弃(re+linqu离开+ish→离开不再要→放弃) 
	- renegade 撤消； 取消（re+peal呼吁→反呼吁→取消；参考：appeal呼吁）  
	- repel 驱除，击退（re+pel推→击退） 
	- repose 休息；躺下（re+plse放→入下〔工作〕→休息） 
	- reprobate 道德败坏之人（re+prob正直＋ate→不正直→败坏）  
	- repugnant 令人厌恶的（re+pugn打＋ant→[把人]打回去→〔行为〕令人厌恶的）
	- resonant 回响的；洪亮的（re+son声音+ant→声音回过来→回响的）

2) 
	- resplendent 辉湟的（re+splend光辉＋[[-ent]]→再光辉→辉煌的）
	- reappear 再出现（re+appear出现）  
	- rearrage 重新安排（re+arrange安排）  
	- reassure 消除某人疑虑（re+assure放心） 
	- recapitulate 重述；概括（re+capit头＋ulate→重新把头拿出来→概括要点）
	- recidivism 重新犯罪（re+cidiv掉下＋ism→再次掉入罪行）  
	- reclaim 取回,回收（re+claim喊→喊回来→取回，引申为开垦荒地）
	- recommend 赞扬；推荐（re+commend赞扬→一再赞扬） 
	- recompense 报酬；赔偿（re+compense补偿）  
	- refurbish 刷新；擦亮（re+furbish装饰→再装饰→刷新） 
	- regenerate 改过自新的（re+gener产生＋ate→重新产生生命→改过自新的） 
	- reincarnate 化生，转生(re+in入＋carn肉＋ate→重新进入肉体→产生)  
	- reinstate 重新恢复职位（re+in+state国家，权力→重新进入权力）  
	- reiterate 重申（re+iterate重说→反复重说） 
	- resurgence 复兴，再起（re+surg浪浪潮＋ence→重起浪潮→复兴）
	- reverberate 起回声，反响（re+verber震动＋ate→重新震动→起回声）






