### 语素含义
英语中以[[-e]]结尾的词极多，它们在==加后缀==时貌似复杂，而实际上却是有规律可依据。
1) 以辅音字母加[[-e]]结尾的词，当后面接以元音字母开头的后缀时，e常省略；
2) 以辅音字母加[[-e]]结尾的词，当后面接以辅音开头的后缀时，e常保留；
3) 以[[-ce]]或[[-ge]]结尾的词，当后面接以a或o开头的后缀时，要保留e；
4) 以[[-ce]]结尾的词，在后面接后缀[[-ous]]时，要把e变为i；
5) 以[[-able]],[[-ible]]或者[[-oble]]结尾的形容词变副词时，应去e加y；
6) 以[[-ee]]结尾的词加后缀时，e不能省略；
7) 以[[-ie]]结尾的动词变现在分词时，应变ie为y加上[[-ing]]；
8) 以[[-oe]]，[[-ye]]结尾的词加后缀时，应保留e；
9) 以[[-ue]]结尾的词，当后面接以元音字母开头的后缀时，e常省略，当后面接以辅音字母开头的后缀时，e常保留；
10) 以[[-e]]结尾的词加[[-ed]]或者[[-er]]常常有两种说法：1.去e再加[[-ed]]或[[-er]]。2.加[[-d]]或[[-r]],其实两种说法都一样，即结尾的[[-e]]要省略；

### 语素来源


### 同源单词
1) 
	- ache -> aching
	- believe ->believe + er -> believer
	- tire -> tire + ed -> tired
[注]
- like -> likable 或 likeable
- move -> movable 或 moveable

2) 
	- hope -> hopeful
	- manage -> management
	- fortunate -> fortunately
	- nine -> ninety
[注]
- judge -> judg(e)ment
- nine -> ninth
- whole -> wholly

3) 
	- manage -> manageable
	- notice ->noticeable
	- courage -> courageous
	- outrage -> outrageous
[注]
- pronounce -> pronunciation
这样做是为了避免读音上的变化，因为c和g常在e和i之前发软音，而在a或o之前发硬音

4) 
	- vice **n.** 恶习；缺点 -> vicious  **adj.** 恶毒的；恶意的
	- grace -> gracious
	- malice -> malicious
	- space -> spacious

5) 
	 - able-> ably
	 - possible -> possibly
	 - comfortable -> comfortably
	 - incredible -> incredibly
	 - noble -> nobly

6) 
	- agree **v.** 同意；赞成；承认 -> agrees ->agreeing -> agreed ->agreement
	- foresee -> foreseeable

7) 
	- die -> dying
	- lie -> lying
	- tie -> tying

8) 
	- hoe -> hoeing -> hoelike
	- toe -> toeing -> toeless
	- dye -> dyeability -> dyeing

9) 
	- cheque -> chequer
	- hue -> hueless
	- league -> leaguing -> leagued
	- sue -> suing -> sued
	- argue -> arguable
[注，以下是一些例外]
- due -> duly 
- true -> truly
- argue -> argument






