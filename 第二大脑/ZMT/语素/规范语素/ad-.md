---
tags:
  - 未完成
  - zmt
语素类别: 
词根含义: 
词根来源: 
同源单词:
---
### 词根含义
来,临近;朝,向,趋向;加强意义
1) 加在同辅音字母的词根前, 含有at, to之意，表示"一再"等加强意义
2) 加在在单词或词根前, 表示"做…, 加强…"

### 词根来源
来源于拉丁语前缀ad-

### 同源单词
1) 
	- advice 忠告，建议，劝告 ( [[ad-]](一再，表示强调) + -vic- (看，注意，理解) + [[-e]] -> 忠告，建议，劝告)
	- addict 上瘾，入迷（ad+dict说→一再说起→对……入迷）  
	- additive 上瘾的（addict的形容词 + [[-ive]]）  
	- adduce 引证，举例（ad+duce引导→一再引导→举例说明）

2) 
	- adapt 适应（ad+apt能力→有适应能力）  
	- adept 熟练的（ad+ept能力→有做事能力→熟练的）
	- adopt 收养；采纳（ad+opt选择→选出来→采纳）  
	- adhere 坚持（ad+here粘→粘在一起→坚持）  
	- adjacent 邻近的（ad+jacent躺→躺在一起→邻近的） 
	- adjoin 贴近；毗连（ad+join参加→参加在一起→贴近）  
	- administrate 管理；执行（ad+ministr部长+[[-ate]]→做部长→管理） 
	- admire 羡慕（ad+mire惊奇→惊喜；羡慕） 
	- adumbrate 预示（ad+umbr影子+[[-ate]]→(将来的)影子出现→预示） 
	- adjust 调整（ad+just+正确→弄正确→调整）  
	- adventure 冒险（ad+venture冒险）  
	- admonish 告诫，警告（ad+mon警告+[[-ish]]→一再警告）  
	- advent 来临，来到（ad+vent来→来到）

