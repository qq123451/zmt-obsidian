### 语素含义
1) 形容词后缀，与名词后缀[[-ence]]或[[-ency]]相对应，表示具有...性质的，关于...的
2) 名词后缀，表示人
3) 名词后缀，表示物（...剂、...药）

### 语素来源


### 同源单词
1) 
	- different 不同的  
	- existent 存在的，现存的 （exist 存在 + -ent → 存在的） 
	- insistent 坚持的 （insist 坚持 + -ent → 存在的） 
	- emergent 紧急的  
	- confident 自信的  
	- persistent 持久的，坚持的  
	- dependent 依赖的  
	- excellent 杰出的  
	- occrurent 偶然发生的  
	- despondent 沮丧的

2) 
	- student 学生 （study 学习 + -ent → 学习的人 → 学生） 
	- resident 居民 （reside 居住 + -ent → 居住的人 → 居民） 
	- correspondent 通讯员  
	- president 总统，大学校长  
	- patient 病人 
	- antecedent 先行者

3) 
	- detergent 洗涤剂 （deterge 使清洁 + -ent → 使清洁的物品 → 使清洁的药剂 → 洗涤剂） 
	- corrodent 腐蚀剂 （corrode 侵蚀，损害 + -ent → 使损害的物品 → 使损害的药剂 → 腐蚀剂） 
	- solvent 溶液  
	- absorbent 吸收剂  
	- abluent 洗净剂  
	- corrigent 矫正药


