---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
名词后缀，构成抽象名词，表示行为、行为的结果、状态、情况

### 语素来源


### 同源单词
- nature 自然，自然界；自然力；自然状态，原始状态；天性，本笥；性质；各类；生命力；人体的本能 （nat ( = to be born 出生) + [[-ure]] 名词后缀 -> 天性 -> 自然状态 -> 自然）
- departure 离开，出发  ([[de-]]  + -part- + [[-ure]])
- pressure 压力，压  (press- + [[-ure]])
- failure 失败  
- exposure 暴露，揭露  
- sculpture 雕刻（品）  
- seizure 抓住，捕捉  
- contracture 挛缩 
- closure 关闭，结束 
- disclosure 泄露  
- moisture 潮湿，湿度 
- creature 创造物，生物  
- procedure 程序，步骤  
- flexure 弯曲 
- pleasure 愉快
