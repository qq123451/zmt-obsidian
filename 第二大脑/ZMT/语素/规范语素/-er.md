---
tags:
  - 未完成
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
1) 名词后缀，表示人，行为的主动者，做某事的人
2) 名词后缀，表示人，与某事物有关的人
3) 名词后缀，表示人，属于某国、某地区的人
4) 名词后缀，表示物（与某事物有关之物或能做某事之物）及动物（能做某事的动物）
5) 名词后缀，加在方位词上表示“...风”
6) 动词后缀，表示反复动作，连续动作及拟声动作
7) 形容词及副词后缀，表示比较级“更...”

### 语素来源


### 同源单词
1) 
	- singer 歌唱家  
	- dancer 跳舞者 
	- teacher 教师  
	- writer 作者，作家 
	- reader 读者  
	- leader 领袖  
	- fighter 战士 
	- worker 工人 
	- farmer 农民  
	- turner 车工
2) 
	- hatter 帽商，制帽工人  
	- banker 银行家  
	- wagoner 驾驶运货车的人 
	- tinner 锡匠  
	- weekender 度周末假者 
	- teenager (十三至十九岁的)青少年 
	- six-footer 身高六英尺以上的人  
	- miler 一英里赛跑运动员
3) 
	- United Stateser 美国人 
	- New Yorker 纽约人  
	- Thailander 泰国人  
	- New Zealander 新西兰人  
	- northerner 北方人  
	- southerner 南方人  
	- Britisher 英国人 
	- Londoner 伦敦人 
	- Icelander 冰岛人  
	- islander 岛国  
	- inlander 内地人  
	- villager 村民
4) 
	- washer 洗衣机  
	- lighter 打火机 
	- heater 加热器  
	- cutter 切削器，刀类 
	- boiler 煮器，锅  
	- fiver 五元钞票  
	- tenner 十元钞票 
	- silencer 消音器  
	- woodpecker 啄木鸟 
	- creeper 爬行动物，爬虫
5) 
	- norther 强北风  
	- northwester 强西北风  
	- souther 强南风  
	- southeaster 强东南风
6) 
	- waver 来回摆动  
	- chatter 喋喋不休  
	- stutter 结舌，口吃  
	- batter 连打  
	- mutter 喃喃自语  
	- clatter 作卡嗒声  
	- patter 发嗒嗒声  
	- whisper 低语，作沙沙声
7) 
	- graeter 更大 
	- warmer 更暖 
	- happier 更快乐 
	- faster 更快  
	- earlier 更早  
	- harder 更努力
