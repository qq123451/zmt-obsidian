### 语素含义
1) 形容词后缀，表示属于...的、具有...性质的、如...的
2) 名词后缀，构成抽象名词，表示行为、状况、事情
3) 名词后缀，表示物

### 语素来源


### 同源单词
1) 
	- natural 自然的；自然界的；自然发生的；天生的，天赋的；自然状态的；天然的，不做作的；正常的；合乎自然规律的  ((nature 自然 + [[-al]] 形容词后缀) -> natural([[-e]]省略) -> 出生的 -> 天生的 -> 天然的 -> 合乎自然规律的 -> 自然(界)的)
	- personal 个人的  
	- prepositional 介词的  
	- autumnal 秋天的  
	- national 国家的，民族的 
	- emotional 感情上的  
	- imaginal 想像的 
	- global 全球的  
	- coastal 海岸的 
	- parental 父母的  
	- invitational 邀请的 
	- frontal 前面的，正面的  
	- conversational 会话的  
	- governmental 政府的  
	- regional 地区的，局部的  
	- continental 大陆的  
	- educational 教育的  
	- exceptional 例外的

2) 
	- criminal 犯罪分子 
	- rival 竞争者 
	- aboriginal 土人 
	- rascal 恶棍，歹徒 
	- corporal 班长，下士  
	- arrival 到达者

3) 
	- mural 壁画 
	- dial 日晷  
	- manual 手册  
	- hospital 医院 
	- arsenal 武器库  
	- urinal 尿壶，小便池 
	- singal 信号  
	- diagonal 对角线
