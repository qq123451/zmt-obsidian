---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
1) 形容词后缀，表示多...的、有...的、如...的、属于...的（大多数加在单音节名词之后）
2) 名词后缀，构成抽象名词，表示性质、状态、情况、行为
3) 名词后缀，表示人或物
4) 名词后缀，表示小称及爱称（在一部分词中与[[-ie]]通用）

### 语素来源


### 同源单词
1) 
	- noisy  adj. 嘈杂的; 喧闹的; 充满噪音的; 吵吵闹闹的   (nois = [[nau, nav, naut, naus|naus]] 船  + [[-e]]  -> noise n. 噪声,响声 + [[-y]] 形容词后缀  -> [[-e]] 省略，noisy 嘈杂的)
	- rainy 下雨的  (rain n. 雨；下雨；雨天；雨季  + [[-y]] 形容词后缀 -> 下雨的)
	- windy 有风的 
	- sunny 阳光充足的
	- hilly 多小山的  
	- rosy 玫瑰色的 
	- woody 树木茂密的
	- snowy 多雪的 
	- silvery 似银的  
	- smoky 多烟的 
	- bloody 血的，流血的
	- watery 多水的，如水的
	- silky 丝一样的  
	- wintery 冬天（似）的
	- sleepy 想睡的  
	- earthy 泥土似的  
	- greeny 略呈绿色的 
	- trustry 可信赖的
	- homey 像家一样的
	- inky 有墨迹的
	- hairy 多毛的 
	- icy 似冰的，多冰的
	- wordy 多言的 
	- woolly 多羊毛的 
	- cloudy 多云的
[注]在以y或o为结尾的词之后，则作[[-ey]]，如skyey天空的，天蓝色的，clayey（多）粘土的，mosquitoey蚊子多的


2) 
	- beauty 美丽 (beaut 美的 + [[-y]] -> 美丽)
	- difficulty 困难  
	- discovery 发现  
	- soldiery 军事训练  
	- inquiry 询问，打听  
	- burglary 夜盗行为  
	- bastardy 私生子身份  
	- mastery 精通，掌握  
	- beggary 乞丐生涯，行乞  
	- modesty 谦虚，虚心  
	- jealousy 妒忌，猜忌  
	- injury 伤害，损害 
	- monotony 单音，单调

3) 
	- lefty 左撇子
	- fatty 胖子 
	- darky 黑人
	- oldy 老人  
	- newsy 报童  
	- nighty 妇女（或孩子）穿的睡衣 
	- towny 城里人，镇民 
	- shorty 矮子 
	- cabby 出租车驾驶人  
	- smithy 铁匠，锻工 
	- sweety 糖果，蜜饯  
	- parky 公园管理人  
	- whitey 白人

4) 
	- doggy （=doggie）小狗 
	- pitggy （=piggie）小猪  
	- kitty 小猫  
	- missy 小姑娘，小姐 
	- daddy 爸爸，爹爹
	- granny （=grannie）奶奶
	- aunty （=auntie） 阿姨  
	- maidy 小女孩






