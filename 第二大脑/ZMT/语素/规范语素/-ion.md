---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
1) 名词后缀，构成抽象名词，表示行为、行为的过程或结果、情况、状态
2) 名词后缀，表示物

### 语素来源


### 同源单词
1) 
	- vacation n. 假期 (vac 空 +  [[-ate]] 动词后缀 ->  vacate v. 空出  + [[-ion]]  ->  [[-e]] 省略，vacation)
	- election 选举  （[[e-, ex-]] 出，向外，向上  +  lect 选择, 收集  ->  elect v. 选出；选举；卓越的 + [[-ion]]）
	- action 活动，作用，行为  
	- discussion 讨论  
	- inflation 通货膨胀  
	- progression 前进，行进  
	- connection 连结  
	- prediction 预言，预告  
	- exhibition 展览会  
	- perfection 完整无缺  
	- translation 翻译  
	- correction 改正  
	- expression 表达，表示  
	- association 联系，协会  
	- dismission 解雇，开除  
	- prosession 占有，占用  
	- completion 完成

2) 
	- medallion 大奖章  (medal n. 勋章，奖章；纪念章 + l + [[-ion]] )
	- accordion 手风琴  (accord n. 符合；一致；协议；自愿 + [[-ion]])
	- falchion 弯形大刀  
	- cushion 坐垫，靠垫



