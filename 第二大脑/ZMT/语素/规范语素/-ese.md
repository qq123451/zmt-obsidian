---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
1) 形容词兼名词后缀，表示某国的、某地的；某国或某地的人及语言
2) 名词后缀，表示某派（或某种）的文体，文风或语言

### 语素来源


### 同源单词
1) 
	- Chinese 中国的（人），汉语  
	- Japanese 日本的（人），日语 
	- Vietnamese 越南的（人、语）  
	- Burmese 缅甸的（人、语）  
	- Maltese 马耳他的（人、语） 
	- Cantonese 广州的（人、语）  
	- Viennese 维也纳的（人）  
	- Congolese 刚果的（人、语）  
	- Milanese 米兰的（人）  
	- Siamese 暹罗的（人、语）  
	- Portuguese 葡萄牙的（人、语）
2) 
	- translationese 翻译文本  
	- officialese 公文体  
	- academese 学院派文体 
	- journalese 新闻文体 
	- Americanese 美国英语  
	- telegraphese 电报文体  
	- childrenese 儿童语言  
	- computerese 计算机语言 
	- educationese 教育界术语  
	- televisionese 电视术语 
	- bureaucratese 官腔  
	- legalese 法律术语