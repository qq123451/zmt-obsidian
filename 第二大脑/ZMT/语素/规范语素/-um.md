---
tags:
  - 未完成
  - zmt
语素类别: 规范语素
词根含义: 
词根来源: 
同源单词:
---
### 语素含义
名词后缀，表示场所、实物及抽象名词

### 语素来源


### 同源单词
- medium 中间物 （-medi-  中间的 + [[-um]]）
- museum 博物馆 
- mausoleum 陵墓
- asylum 避难所，收容所
- forum 法庭，论坛  
- sanctum 圣所，私室  
- minimum 最小量
- plenum 充满，全体会议
- symposium 酒会，座谈会，讨论会
- rectum 直肠
- hypogeum 地下室，窖
- contagium 接触传染物
- vacuum 真空（状态），真实度
