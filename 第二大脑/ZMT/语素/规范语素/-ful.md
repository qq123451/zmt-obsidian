### 语素含义
1) 名词后缀，加在名词之后，表示充满时的量
2) 形容词后缀，表示富有...的，充满...的，具有...性质的，易于...的，可...的


### 语素来源


### 同源单词
1) 
	- handful 一把，一把的量 (hand 手  + [[-ful]] 表示充满时的量 -> 一把的量)
	- spoonful 一匙的量  (spoon 匙，勺子  + [[-ful]] 表示充满时的量 -> 一匙的量)
	- houseful 满屋，一屋子 
	- armful 一抱  
	- bagful 一满袋  
	- drawerful 一抽屉 
	- cupful 一满杯  
	- mouthful 一口  
	- dishful 一喋的量 
	- boatful 一船所载的量  
	- boxful 一满箱，一满盒 
	- bellyful 一满腹

2) 
	- beautiful 美丽的 (beaut adj. 美的，美丽的 + [[-y]] 名词后缀 + [[-ful]]   -> beauty n. 美，美丽 + [[-ful]] ->beauti (后缀[[-i]]由[[-y]]变形而来) + [[-ful]] -> 美丽的)
	- useful 有用的 (use n. 使用，用途 + [[-ful]] -> 有使用的，有用途的 -> 有用的)
	- fruitful 有结果的 
	- hopeful 富有希望的  
	- dreamful 多梦的  
	- peaceful 和平的 
	- powerful 有力的  
	- sorrowful 悲哀的 
	- changeful 多变化的 
	- shameful 可耻的 
	- fearful 可怕的 
	- forgetful 易忘的 
	- truthful 真实的  
	- skillful 熟练的  
	- doubtful 可疑的 
	- careful 小心的  
	- cheerful 快乐的

