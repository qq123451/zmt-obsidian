---
tags: 
english: 
aliases: 
synonym: 
hypernym: 
hyponym: 
relate:
---
### 定义
**整式**为[[单项式]]和[[多项式]]的==统称==，是[[有理式]]的一部分，在[[有理式]]中可以包含==加、减、乘、除、乘方五种运算==，但在**整式**中除数不能含有[[字母]]。