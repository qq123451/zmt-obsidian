---
tags: 
english: 
aliases: 
synonym: 
hypernym: 
hyponym: 
relate:
---
### 定义
**代数式**，是由[[数]]和表示数的[[字母]]经过有限次加、减、乘、除、乘方和开方等[[代数运算]]所得的[[式子]]，或含有字母的==数学表达式==称为**代数式**。例如：ax+2b，b^2/26，√a+√2等。
单独一个[[字母]]或一个[[数]]也是**代数式**。

[[代数式的值]]


#### 分类
在实数范围内，**代数式**分为[[有理式]]和[[无理式]]。
